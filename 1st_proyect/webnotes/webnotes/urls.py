from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

from django.conf.urls.defaults import *

import views

urlpatterns = patterns('',

    url(r'^list/$', views.notes_list, name='notes_list'),

    url(r'^detail/(?P<id>\d+)/$', views.notes_detail, name='notes_detail'),
    
    url(r'^notes/', include('notes.urls')),

    url(r'^admin/', include(admin.site.urls)),

    )



#urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'webnotes.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

#    url(r'^admin/', include(admin.site.urls)),
#)
